let input = document.querySelector (".todo-input");
let button = document.querySelector (".button");
let list = document.querySelector (".todo-list");
let filter = document.querySelector ("#filter-select");

button.addEventListener ("click", (event) => {
    event.preventDefault();

    const todoDiv = document.createElement ("div");
    todoDiv.classList.add ("todoDiv");

    const todoLi = document.createElement ("li");
    todoLi.classList.add ("todoLi");
    todoLi.innerText = input.value;

    saveLocalTodo (input.value)

    todoDiv.appendChild (todoLi);
    input.value = "";

    const check = document.createElement ("button");
    check.classList.add ("check-btn")
    check.innerHTML = '<i class="fa fa-check"></i>';

    const trash = document.createElement ("button");
    trash.classList.add ("trash-btn");
    trash.innerHTML = '<i class="fa fa-trash-o"></i>';

    todoDiv.appendChild (check);
    todoDiv.appendChild (trash);

    list.appendChild (todoDiv);
});

list.addEventListener ("click", (event) => {
    const item = event.target;
    console.log (item);

    if (item.classList[0] === "trash-btn") {
        const todo = item.parentElement;
        removeLocalTodo (todo);
        todo.remove ();
    }
    if (item.classList[0] === "check-btn") {
        const todo = item.parentElement;
        todo.classList.toggle("complete");
    }
});

filter.addEventListener ("click", (event) => {
    const todos = list.childNodes;
    todos.forEach ( (todo) => {
        switch (event.target.value) {
            case "all" : 
                todo.style.display ="flex";
            break;
            case "completed" :
                if (todo.classList.contains ("complete")) {
                    todo.style.display = "flex";
                }else {
                    todo.style.display = "none";
                }
            break;
            case "uncompleted" :
                if (todo.classList.contains ("complete")) {
                    todo.style.display = "none";
                }else {
                    todo.style.display = "flex";
                }
            break;
        }
    })
})

function saveLocalTodo (todo) {
    let todos;
    if (localStorage.getItem("todos") === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem("todos"))
    }
    todos.push(todo);
    localStorage.setItem("todos", JSON.stringify(todos));
}

function removeLocalTodo (todo) {
    let todos ;
    if (localStorage.getItem("todos") === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem("todos"))
    };
    const todoIndex = todo.children[0].innerText;
    todos.splice (todos.indexOf(todoIndex),1);
    localStorage.setItem("todos", JSON.stringify(todos));
}

document.addEventListener("DOMContentLoaded", () => {
    let todos;
    if (localStorage.getItem("todos") === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem("todos"));
    };
    todos.forEach(function (item) {
    const todo = document.createElement ("div");
    todo.classList.add ("todo")
    const li = document.createElement ("li")
    li.innerText = item;
    
    todo.appendChild (li);
    input.value = "";

    const check = document.createElement ("button");
    check.inenerHTML = '<i class="fa fa-check"></i>';
    check.classList.add("check-btn");
    todo.appendChild (check);

    const trash = document.createElement ("button");
    trash.classList.add("trash-btn");
    trash.inenerHTML ='<i class="fa fa-trash-o"></i>';
    todo.appendChild (trash);

    works.appendChild(todo);

})
})